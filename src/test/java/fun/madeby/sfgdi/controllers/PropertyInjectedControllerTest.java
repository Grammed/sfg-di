package fun.madeby.sfgdi.controllers;

import fun.madeby.sfgdi.services.ConstructorInjectedGreetingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PropertyInjectedControllerTest {

	PropertyInjectedController controller;

@BeforeEach
void setUp() {
	controller = new PropertyInjectedController();
	// Mimics what SF would do with Setter Property Injected dependency
	controller.greetingService = new ConstructorInjectedGreetingService();
}

@Test
void getGreeting() {
	System.out.println(controller.getGreeting());
}
}