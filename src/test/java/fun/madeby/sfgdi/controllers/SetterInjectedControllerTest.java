package fun.madeby.sfgdi.controllers;

import fun.madeby.sfgdi.services.ConstructorInjectedGreetingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SetterInjectedControllerTest {

	SetterInjectedController controller;

@BeforeEach
void setUp() {
	controller = new SetterInjectedController();
	//Mimics what SF would be doing with setter injected dependence
	controller.setGreetingService(new ConstructorInjectedGreetingService());
}

@Test
void getGreeting() {
	System.out.println(controller.getGreeting());
}
}