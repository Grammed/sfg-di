package fun.madeby.sfgdi.controllers;

import fun.madeby.sfgdi.services.ConstructorInjectedGreetingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ConstructorInjectedControllerTest {
	ConstructorInjectedController controller;

@BeforeEach
void setUp() {
	//Mimics what SF would be doing with Constructor injected dependency
	controller = new ConstructorInjectedController(new ConstructorInjectedGreetingService());
}

@Test
void getGreeting() {
	System.out.println(controller.getGreeting());
}
}