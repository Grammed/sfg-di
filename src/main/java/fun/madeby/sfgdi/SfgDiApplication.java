package fun.madeby.sfgdi;

import fun.madeby.sfgdi.controllers.ConstructorInjectedController;
import fun.madeby.sfgdi.controllers.MyController;
import fun.madeby.sfgdi.controllers.PropertyInjectedController;
import fun.madeby.sfgdi.controllers.SetterInjectedController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SfgDiApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(SfgDiApplication.class, args);

		// Instead of creating ask Spring to provide MyController object.
		MyController myController = (MyController)  ctx.getBean("myController");



		System.out.println("------------------Primary Bean");
		System.out.println(myController.sayHello());

		System.out.println("------------------Property");

		PropertyInjectedController propertyInjectedController =
			(PropertyInjectedController) ctx.getBean("propertyInjectedController");

		System.out.println(propertyInjectedController.getGreeting());

		System.out.println("------------------Setter");

		SetterInjectedController setterInjectedController =
			(SetterInjectedController) ctx.getBean("setterInjectedController");


		System.out.println(setterInjectedController.getGreeting());


		System.out.println("------------------Constructor");

		ConstructorInjectedController constructorInjectedController =
			(ConstructorInjectedController) ctx.getBean("constructorInjectedController");

		System.out.println(constructorInjectedController.getGreeting());
	}

}
